resource "aws_s3_bucket" "bluelightco-test-one" {
  bucket = "bluelightco-test-one"
}

resource "aws_s3_bucket" "bluelightco-test-two" {
  bucket = "bluelightco-test-two"
}

resource "aws_s3_bucket" "bluelightco-test-three" {
  bucket = "bluelightco-test-three"
}

resource "aws_s3_bucket" "bluelightco-test-four" {
  bucket = "bluelightco-test-four"
}
